<?php
/* 
 * Plugin Name: Import Co-Authors
 * 
 * Description: This is a plugin that uses a PHP Class and JS Object to provide a timepicker with AJAX Strtotime Support. The idea here is to provide a clean and consistent experience for both developers and users when there arises a need to input time data in the WordPress backend.
 * Version: 0.5
 * Author: Taylor Young <taylor@x-team.com>
 * 
 */

class WPized_TimePicker {
	const AJAX_NONCE_ACTION = 'leadin-strtotime';

	static function setup() {
		add_action( 'wp_ajax_leadin_text_strtotime', array( __CLASS__, '_handle_strtotime_request' ) );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, '_register_scripts_styles' ) );
	}

	/**
	 * Register Jquery UI Theme and Timepicker Styles
	 */
	static function _register_scripts_styles() {
		wp_register_script(
			'jquery-ui-sliderAccess'
			, '/wp-content/includes/wpized_base/plugins/timepicker/libs/jquery.ui.sliderAccess.js'
			, array()
			, '0.3'
			, false
		);
		wp_register_script(
			'jquery-ui-timepicker'
			, '/wp-content/includes/wpized_base/plugins/timepicker/libs/jquery.ui.timepicker.addon.js'
			, array( 'jquery-ui-datepicker', 'jquery-ui-slider', 'jquery-ui-sliderAccess', )
			, '1.2'
			, false
		);
		wp_register_script(
			'wpized-timepicker'
			, '/wp-content/includes/wpized_base/plugins/timepicker/timepicker.js'
			, array( 'jquery-ui-timepicker', )
			, '0.1'
			, false
		);
		wp_register_style(
			'timepicker'
			, '/wp-content/includes/wpized_base/plugins/timepicker/css/jquery.ui.timepicker.addon.css'
			, array( 'jquery-ui-smoothness', )
			, '1.2'
		);
		wp_register_style(
			'jquery-ui-smoothness'
			, '/wp-content/includes/wpized_base/plugins/timepicker/css/jquery-ui-smoothness/jquery-ui-1.10.2.custom.min.css'
			, array()
			, '1.10.2'
		);
	}

	/**
	 * Ajax StrToTime responder takes a string, and returns values for making a JS Date object
	 */
	static function _handle_strtotime_request() {
		if ( empty( $_GET['security'] ) || !wp_verify_nonce( $_GET['security'], self::AJAX_NONCE_ACTION ) ) {
			wp_send_json_error();
		}
		if ( empty( $_GET['str' ] ) ) {
			wp_send_json_error();
		}
		$json = array();
		date_default_timezone_set( get_option( 'timezone_string' ) );
		$timestamp = strtotime( $_GET['str'] );
		if ( ! empty( $timestamp ) ) {
			$json['year']  = date( 'Y', $timestamp );
			$json['month'] = date( 'n', $timestamp ) - 1;
			$json['date']  = date( 'j', $timestamp );
			$json['hrs']   = date( 'H', $timestamp );
			$json['min']   = date( 'i', $timestamp );
			$json['sec']   = date( 's', $timestamp );
		}
		date_default_timezone_set( 'UTC' );
		if ( empty($json) ) {
			wp_send_json_error();
		}
		else {
			wp_send_json_success( $json );
		}
	}

	/**
	 * Convert timepicker format to timestamp
	 */
	static function timepicker_to_timestamp( $timepicker_date ) {
		date_default_timezone_set( get_option( 'timezone_string' ) );
		$timestamp = strtotime( str_replace( ' @ ', ' ', preg_replace( '/\([^)]+\)/', '', $timepicker_date ) ) );
		date_default_timezone_set( 'UTC' );
		return $timestamp;
	}

	/**
	 * Convert timestamp to timepicker format
	 */
	static function timestamp_to_timepicker( $timestamp ) {
		if( $timestamp != '' ) {
			date_default_timezone_set( get_option( 'timezone_string' ) );
			$timepicker_date = date( 'D F j, Y @ h:i a (O)', $timestamp );
			date_default_timezone_set( 'UTC' );
			return $timepicker_date;
		}else{
			return;
		}
	}

}
