var WPized_TimePicker = {
	$datepicker        : null
	, current_time     : null
	, timezone_string  : null
	, timezone_offset  : null
	, ajax_nonce       : null
	, ajax_fail_msg    : null
	, current_timezone : null
	, time_updated     : null
	, init : function(config) {
		$datepicker           = config.datepicker
		this.current_timezone = config.current_timezone;
		this.current_time     = config.current_time;
		this.timezone_string  = config.timezone_string;
		this.timezone_offset  = config.timezone_offset;
		this.ajax_nonce       = config.ajax_nonce;
		this.ajax_fail_msg    = config.ajax_fail_msg;
		this.time_updated     = config.time_updated; //Callback, sent date/time picker value. ( date )
		this.current_timezone = this.convertOffset(this.timezone_offset);
		$datepicker.on('click', function() {
			this.select();
		});
		$datepicker.datetimepicker({
			dateFormat         : 'D MM d, yy'
			, separator        : ' @ '
			, timeFormat       : 'hh:mm tt (z)'
			, showTimezone     : true
			, timezoneList     : [ { 'value': this.current_timezone, 'label': this.timezone_string } ]
			, defaultTimezone  : this.current_timezone
			, hourGrid         : 4
			, minuteGrid       : 15
			, stepMinute       : 5
			, addSliderAccess  : true
			, sliderAccessArgs : { touchonly: true }
			, onClose : function(dateText, elem) {
				WPized_TimePicker.strtotime( dateText );
			}
		});
		this.time_updated( $datepicker.datetimepicker("getDate") );
	}
	, convertOffset : function (gmt_offset) {
		var time = gmt_offset.toString().split(".")
			, hour = parseInt(time[0])
			, negative = hour < 0 ? true : false;
		hour = Math.abs(hour) < 10 ? "0" + Math.abs(hour) : Math.abs(hour);
		hour = negative ? "-" + hour : "+" + hour;
		return time[1] ? hour+(time[1]*6).toString() : hour + "00";
	}
	, strtotime : function (date) {
		if( date.indexOf('@') === -1 ) {
			jQuery.ajax({
				url: ajaxurl
				, data: {
					str        : date
					, day      : new Date().getDate()
					, action   : 'leadin_text_strtotime'
					, security : this.ajax_nonce
				}
			}).done( function(response){
				if( response.data === '' || response.success === false ) {
					$datepicker.attr('value', '').attr('placeholder', WPized_TimePicker.ajax_fail_msg );
				} else {
					$datepicker.datetimepicker(
						'setDate'
						, new Date(
							response.data.year
							, response.data.month
							, response.data.date
							, response.data.hrs
							, response.data.min
							, response.data.sec
						) 
					);
				}
				WPized_TimePicker.time_updated( $datepicker.datetimepicker("getDate") );
			});
		}else{
			this.time_updated( $datepicker.datetimepicker("getDate") );
		}
	}
}