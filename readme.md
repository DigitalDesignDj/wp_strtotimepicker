# WordPress StrToTimePicker

This is a plugin that uses a PHP Class and JS Object to provide a timepicker with AJAX Strtotime Support. The idea here is to provide a clean and consistent experience for both developers and users when there arises a need to input time data in the WordPress backend.

See leadin-text.php on rogers_news for an example of use. (`themes/rogers-news/includes`)

 * Works with the WPized_TimePicker PHP Class
 * Standard Date/Time Format
 * AJAX StrToTime
 * Saves Timestamp to DB
 * Uses WordPress Timezone

## The Time Format

Since we have a nice gui for both mouse driven and keyboard inclined users, no one should be actually inputting the entire time format by hand. Therefore the time format chosen by this plugin has been designed to be easily readable, and is not (currently) configurable. The plugin uses the WordPress timezone setup in Settings > General

Here's what it looks like to humans:

	Sat April 6, 2013 @ 12:45 am (-0400)

in jQueryUI/TimePicker JavaScript:

	// Datepicker + Timepicker configuration
	'D MM d, yy @ hh:mm tt (z)'

in PHP:

	// Date() Formatting
	'D F j, Y @ h:i a (O)'

and to computers / the DB:

	// Unix Timestamp
	1365227100

## PHP Class

There are 2 methods of the PHP class that will need to use to convert the data from the date/time-picker to the preferred UNIX timestamp format `timepicker_to_timestamp()` and `timestamp_to_timepicker()`.

They work as advertised and do exactly as the labels say.

### Timepicker String to Timestamp

It is possible that the timepicker JS will not parse the date when the data is submitted, therefore your server-side parsing should include a check for this case. I suggest checking for the @ symbol, because users will not be using it in thier date/time strings for parsing via AJAX.

Here's an example:

	if ( strpos( $timepicker_data, ' @ ') === false ) :
		// We have a user string (to time) submitted for saving to the db, use strtotime()
		$timestamp = strtotime( $timepicker_data );
	else :
		// Looks like the data came from the timepicker, parse it to a timestamp
		$timestamp = WPized_TimePicker::timepicker_to_timestamp( $timepicker_data );
	endif;
	// Save it
	update_post_meta( $post->ID, 'a_timestamp', $timestamp );

But what if strtotime fails?

	if ( strpos( $timepicker_data, ' @ ') === false ) :
		if ( ( $timestamp = strtotime( $timepicker_data ) ) === false) :
			$timestamp = '';
		endif;
	else :
		$timestamp = WPized_TimePicker::timepicker_to_timestamp( $timepicker_data );
	endif;
	update_post_meta( $post->ID, 'a_timestamp', $timestamp );

There we are, a unix timestamp `$timestamp` saved to post_meta. (CopyPasta ready)

### Timestamp to Timepicker String

Now that we have a timestamp in the DB, we need to go the other way, and populate the date/time-picker with a value from the database. It's easy:

	<?php $datetime_str = WPized_TimePicker::timestamp_to_timepicker( get_post_meta( $post->ID, 'a_timestamp', true ) ) ?>
	<input type="text" id="time" name="time" value="<?php echo esc_attr( $datetime_str ) ?>" />

Both methods use the local timezone setup in WordPress to parse time. (Need to do some testing here, getting odd results.)

For best results, look at the functions in the source code, the're dead simple.

## JavaScript Object/Class

### Dependencies

 * jQuery (Included with WordPress)
 * jQuery UI Datepicker (Included with WordPress)
 * jQuery UI Timepicker AddOn (`/libs`)
 * jQuery UI Slider Access AddOn (`/libs`) - Touchscreen Support for Sliders

 * Timepicker CSS (`/css`)
 * jQuery UI Theme "Smoothness" (`/css`)

Include the needed JS & CSS like this, they are registered in the WPized_TimePicker::_register_scripts_styles method

	wp_enqueue_style( 'timepicker' );
	wp_enqueue_script( 'wpized-timepicker' );

### Options

All options with the exception of time_updated are currently __required__

 * datepicker      - <input> element to attach date/time picker to
 * current_time    - data from WordPress
 * timezone_string - data from WordPress
 * timezone_offset - data from WordPress
 * ajax_nonce      - data from WordPress
 * ajax_fail_msg   - data from WordPress
 * time_updated    - callback function, use this to update interface elements via JS

### Example

	jQuery(document).ready(function($) {
		WPized_TimePicker.init({
			datepicker        : $('#rogers_leadin_text_expiration')
			, current_time    : <?php echo json_encode( current_time( 'timestamp' ) ) ?>
			, timezone_string : <?php echo json_encode( get_option( 'timezone_string' ) ) ?>
			, timezone_offset : <?php echo json_encode( get_option( 'gmt_offset' ) ) ?>
			, ajax_nonce      : <?php echo json_encode( WPized_TimePicker::AJAX_NONCE_ACTION ); ?>
			, ajax_fail_msg   : <?php echo json_encode( __( 'I don\'t understand, please try again', ROGERS_LOCALE ) ); ?>
			, time_updated    : function ( date ) {}
		});
	});

### Advanced Use

Call the time_updated callback from outside the object, passing the correct value into the function

	WPized_TimePicker.time_updated( WPized_TimePicker.$datepicker.datetimepicker("getDate") );

`WPized_TimePicker.$datepicker.datetimepicker("getDate")` returns the current timepicker value
